/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package file;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Product;
import models.SupermarketArea;

/**
 *
 * @author Cao
 */
public class FileManager {
    private ArrayList<Product> productDel = null;
    private SupermarketArea areaSup = null;
    private void LoadInfo(String strLine,ArrayList<Product> lstProducts){
        String name = strLine.substring(strLine.indexOf("(") + 1, strLine.indexOf(")"));
        String s1 = strLine.substring(0, strLine.indexOf("(")).trim();
        String s2 = strLine.substring(strLine.indexOf(")") + 2, strLine.length()).trim();
        String[] productinfo1 = s1.split("\\s");
        String[] productinfo2 = s2.split("\\s");
        Product product = new Product();
        product.setPosRow(Integer.parseInt(productinfo1[0]));
        product.setPosCol(Integer.parseInt(productinfo1[1]));
        product.setIDProduct(productinfo1[2]);
        product.setNameProduct(name);
        product.setPrice(Double.parseDouble(productinfo2[0]));
        product.setDate(productinfo2[1]);
        product.setEndDay(productinfo2[2]);
        product.setContainer(Integer.parseInt(productinfo2[3]));
        product.setColorCode(Integer.parseInt(productinfo2[4]));      
        lstProducts.add(product);
        Logger.getLogger(FileManager.class.getName()).info(strLine);
    }
    public void readFile(String filename, ArrayList<SupermarketArea> areas){
        try {
            FileInputStream fstream = new FileInputStream(filename);
            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String strLine;
            boolean isArea = false;
            int areaCode = 0;
            while ((strLine = br.readLine()) != null){
                if(isArea){
                    if(!strLine.equals("")){
                        Logger.getLogger(FileManager.class.getName()).info(strLine);
                        String[] info = strLine.split("\\s");
                        SupermarketArea area = new SupermarketArea();
                        area.setPosX(Integer.parseInt(info[0]));
                        area.setPosY(Integer.parseInt(info[1]));
                        area.setAngle(Integer.parseInt(info[2]));
                        area.setShelfquantity(Integer.parseInt(info[3]));
                        area.setShelfheight(Integer.parseInt(info[4]));
                        area.setDrawernumber(Integer.parseInt(info[5]));
                        int productQuantity = Integer.parseInt(info[6]);
                        ArrayList<Product> lstProducts = new ArrayList<Product>();
                        for(int quantity = 0; quantity < productQuantity; quantity++){
                            strLine = br.readLine();
                            LoadInfo(strLine, lstProducts);
                        }
                        area.setListProducts(lstProducts);
                        areas.add(area);
                    }
                   isArea = false;
                }
                if(areaCode == 0){
                    isArea = true;
                    areaCode++;
                }
                if(strLine.equals("")){
                    isArea = true;
                }
            }
            in.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FileManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void WriteFile(String file, ArrayList<SupermarketArea> areas) throws IOException{        

	try {
            File f = new File(file);
            FileWriter fw = new FileWriter(f.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            String size  = String.valueOf(areas.size());
            bw.write(size);
            bw.write(System.lineSeparator());
            for(int i=0; i<areas.size(); i++){
                areaSup = areas.get(i); 
                productDel = areaSup.getListProducts();
                String value  = areaSup.getPosX()+ " " + areaSup.getPosY() + " "
                        + areaSup.getAngle() + " " + areaSup.getShelfquantity() + " "
                        + areaSup.getShelfheight() + " " + areaSup.getDrawernumber() + " "
                        + productDel.size();
                bw.write(value);

                bw.write(System.lineSeparator());

                for(int j=0; j<productDel.size(); j++){
                    Product product = productDel.get(j);
                    String info = product.getPosRow() + " " + product.getPosCol() + " " 
                            + product.getIDProduct() + " (" + product.getNameProduct() + ") "
                            + product.getPrice() + " "  
                            + product.getDate() + " " + product.getEndDay() + " "
                            + product.getContainer() + " " + product.getColorCode();
                    bw.write(info);
                    
                    bw.write(System.lineSeparator());
                }
                bw.write(System.lineSeparator());
            }
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
	}
    }
    
    public void OuputFile(String file, ArrayList<String> lstTransaction)
    {
        try {
            File f = new File(file);
            FileWriter fw = new FileWriter(f,true);
            String value = "";
            for(int i = 0; i < lstTransaction.size(); i++)
            {
                value += lstTransaction.get(i);
                if(i != lstTransaction.size() - 1)
                {
                    value += " ";
                }
            }
            value += "\n";
            fw.write(value);
            fw.close();
        } catch (IOException ex) {
            Logger.getLogger(FileManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
