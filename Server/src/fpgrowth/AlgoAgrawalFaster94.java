package fpgrowth;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class AlgoAgrawalFaster94{
	
        public static ArrayList<String> lstSValue= new ArrayList<String>();
	protected Itemsets patterns;
	protected AssocRules rules;
	protected BufferedWriter writer = null;
	protected long startTimestamp = 0;
	protected long endTimeStamp = 0;
	protected int ruleCount = 0;
	protected int databaseSize = 0;
	protected double minconf;
	protected double minlift;
	protected boolean usingLift = true;
	public AlgoAgrawalFaster94(){
		
	}
	public AssocRules runAlgorithm(Itemsets patterns, String output, int databaseSize, double minconf) throws IOException {
		this.minconf = minconf;
		this.minlift = 0;
		usingLift = false;
		return runAlgorithm(patterns, output, databaseSize);
	}

	public AssocRules runAlgorithm(Itemsets patterns, String output, int databaseSize, double minconf,
			double minlift) throws IOException {
		this.minconf = minconf;
		this.minlift = minlift;
		usingLift = true;
		return runAlgorithm(patterns, output, databaseSize);
	}
        
	private AssocRules runAlgorithm(Itemsets patterns, String output, int databaseSize)
			throws IOException {
		if(output == null){
			writer = null;
			rules =  new AssocRules("ASSOCIATION RULES");
	    }else{ 
	    	rules = null;
			writer = new BufferedWriter(new FileWriter(output)); 
		}
		this.databaseSize = databaseSize;
		startTimestamp = System.currentTimeMillis();
		ruleCount = 0;
		this.patterns = patterns;
		for(List<Itemset> itemsetsSameSize : patterns.getLevels()){
			Collections.sort(itemsetsSameSize, new Comparator<Itemset>() {
				@Override
				public int compare(Itemset o1, Itemset o2) {
					return ArraysAlgos.comparatorItemsetSameSize.compare(o1.getItems(), o2.getItems());
				}
			});
		}
		for (int k = 2; k < patterns.getLevels().size(); k++) {
			for (Itemset lk : patterns.getLevels().get(k)) {
				List<int[]> H1_for_recursion = new ArrayList<int[]>();
				for(int item : lk.getItems()) {
					int itemsetHm_P_1[] = new int[] {item};
					int[] itemset_Lk_minus_hm_P_1 = ArraysAlgos.cloneItemSetMinusOneItem(lk.getItems(), item);
					int support = calculateSupport(itemset_Lk_minus_hm_P_1);
					double supportAsDouble = (double) support;
					double conf = lk.getAbsoluteSupport() / supportAsDouble;
					if(conf < minconf || Double.isInfinite(conf)){
						continue;
					}
					double lift = 0;
					int supportHm_P_1 = 0;
					if(usingLift){
						supportHm_P_1 = calculateSupport(itemsetHm_P_1);
						double term1 = ((double)lk.getAbsoluteSupport()) /databaseSize;
						double term2 = supportAsDouble /databaseSize;
						double term3 = ((double)supportHm_P_1 / databaseSize);
						lift = term1 / (term2 * term3);
						if(lift < minlift){
							continue;
						}
					}
					saveRule(itemset_Lk_minus_hm_P_1, support, itemsetHm_P_1, supportHm_P_1, lk.getAbsoluteSupport(), conf, lift);
					H1_for_recursion.add(itemsetHm_P_1);
				}
				apGenrules(k, 1, lk, H1_for_recursion);
			}
			
		}
		if(writer != null){
			writer.close();
		}
		endTimeStamp = System.currentTimeMillis();
		return rules;
	}

	private void apGenrules(int k, int m, Itemset lk, List<int[]> Hm)
			throws IOException {
		if (k > m + 1) {
			List<int[]> Hm_plus_1_for_recursion = new ArrayList<int[]>();
			List<int[]> Hm_plus_1 = generateCandidateSizeK(Hm);
			for (int[] hm_P_1 : Hm_plus_1) {
				int[] itemset_Lk_minus_hm_P_1 =  ArraysAlgos.cloneItemSetMinusAnItemset(lk.getItems(), hm_P_1);
				int support = calculateSupport(itemset_Lk_minus_hm_P_1); 				
				double supportAsDouble = (double)support;				
				double conf = lk.getAbsoluteSupport() / supportAsDouble;
				if(conf < minconf || Double.isInfinite(conf)){
					continue;
				}				
				double lift = 0;
				int supportHm_P_1 = 0;
				if(usingLift){
					supportHm_P_1 = calculateSupport(hm_P_1);  
					double term1 = ((double)lk.getAbsoluteSupport()) /databaseSize;
					double term2 = (supportAsDouble) /databaseSize;
					 lift = term1 / (term2 * ((double)supportHm_P_1 / databaseSize));
					if(lift < minlift){
						continue;
					}
				}
				saveRule(itemset_Lk_minus_hm_P_1, support, hm_P_1, supportHm_P_1, lk.getAbsoluteSupport(), conf, lift);
				if(k != m+1) {
					Hm_plus_1_for_recursion.add(hm_P_1);
				}
			}
			apGenrules(k, m + 1, lk, Hm_plus_1_for_recursion);
		}
	}
        
	private int calculateSupport(int[] itemset) {
	List<Itemset> patternsSameSize = patterns.getLevels().get(itemset.length);
        int first = 0;
        int last = patternsSameSize.size() - 1;
       
        while( first <= last )
        {
        	int middle = ( first + last ) >>1 ;
        	int[] itemsetMiddle = patternsSameSize.get(middle).getItems();

        	int comparison = ArraysAlgos.comparatorItemsetSameSize.compare(itemset, itemsetMiddle);
            if(comparison  > 0 ){
            	first = middle + 1;
            }
            else if(comparison  < 0 ){
            	last = middle - 1;
            }
            else{
            	return patternsSameSize.get(middle).getAbsoluteSupport();
            }
        }
        return 0;
	}
	protected List<int[]> generateCandidateSizeK(List<int[]> levelK_1) {
		List<int[]> candidates = new ArrayList<int[]>();
		loop1: for (int i = 0; i < levelK_1.size(); i++) {
			int[] itemset1 = levelK_1.get(i);
			loop2: for (int j = i + 1; j < levelK_1.size(); j++) {
				int[] itemset2 = levelK_1.get(j);
				for (int k = 0; k < itemset1.length; k++) {
					if (k == itemset1.length - 1) {
						if (itemset1[k] >= itemset2[k]) {
							continue loop1;
						}
					}
					else if (itemset1[k] < itemset2[k]) {
						continue loop2;
					} else if (itemset1[k] > itemset2[k]) {
						continue loop1;
					}
				}
				int lastItem1 =  itemset1[itemset1.length -1];
				int lastItem2 =  itemset2[itemset2.length -1];
				int newItemset[];
				if(lastItem1 < lastItem2) {
					newItemset = new int[itemset1.length+1];
					System.arraycopy(itemset1, 0, newItemset, 0, itemset1.length);
					newItemset[itemset1.length] = lastItem2;
					candidates.add(newItemset);
				}else {
					newItemset  = new int[itemset1.length+1];
					System.arraycopy(itemset2, 0, newItemset, 0, itemset2.length);
					newItemset[itemset2.length] = lastItem1;
					candidates.add(newItemset);
				}

			}
		}
		return candidates; 
	}

	public void printStats() {
		System.out.println("=============  ASSOCIATION RULE GENERATION v0.96f- STATS =============");
		System.out.println(" Number of association rules generated : " + ruleCount);
		System.out.println(" Total time ~ " + (endTimeStamp - startTimestamp) + " ms");
		System.out.println("===================================================");
	}

	protected void saveRule(int[] itemset1, int supportItemset1, int[] itemset2, int supportItemset2,
			int absoluteSupport, double conf, double lift) throws IOException {
		ruleCount++;
		if(writer != null){
			StringBuilder buffer = new StringBuilder();
			for (int i = 0; i < itemset1.length; i++) {
				buffer.append(AlgoFPGrowth.mapElse.get(itemset1[i]));
				if (i != itemset1.length - 1) {
					buffer.append(" ");
				}
			}
			buffer.append(" ==> ");
			for (int i = 0; i < itemset2.length; i++) {
				buffer.append(AlgoFPGrowth.mapElse.get(itemset2[i]));
				if (i != itemset2.length - 1) {
					buffer.append(" ");
				}
			}
			buffer.append(" #SUP: ");
			buffer.append(absoluteSupport);
			buffer.append(" #CONF: ");
			buffer.append(conf);
			if(usingLift){
				buffer.append(" #LIFT: ");
				buffer.append(lift);
			}
			
			writer.write(buffer.toString());
			writer.newLine();
                        lstSValue.add(buffer.toString());
		}
		else{
			rules.addRule(new AssocRule(itemset1, itemset2, supportItemset1, absoluteSupport, conf, lift));
		}
	}
}
