 package fpgrowth;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AlgoFPGrowth {
	private long startTimestamp; 
	private long endTime; 
	private int transactionCount = 0; 
	private int itemsetCount; 

	public int minSupportRelative;
	
	BufferedWriter writer = null; 
	protected Itemsets patterns = null;
	final int BUFFERS_SIZE = 2000;
	private int[] itemsetBuffer = null;
	private FPNode[] fpNodeTempBuffer = null;
	private int[] itemsetOutputBuffer = null;
        Map<String, Integer> mapValue = new HashMap<String, Integer>();
        public static ArrayList<String> lstSValue= new ArrayList<String>();
        public static Map<Integer, String> mapElse = new HashMap<Integer, String>();
	public AlgoFPGrowth() {
		
	}

	public Itemsets runAlgorithm(String input, String output, double minsupp) throws FileNotFoundException, IOException {
		startTimestamp = System.currentTimeMillis();
		itemsetCount = 0;
		if(output == null){
			writer = null;
			patterns =  new Itemsets("FREQUENT ITEMSETS");
	    }else{ 
			patterns = null;
			writer = new BufferedWriter(new FileWriter(output)); 
			itemsetOutputBuffer = new int[BUFFERS_SIZE];
		}
		final Map<Integer, Integer> mapSupport = scanDatabaseToDetermineFrequencyOfSingleItems(input); 

		this.minSupportRelative = (int) Math.ceil(minsupp * transactionCount);

		FPTree tree = new FPTree();
		BufferedReader reader = new BufferedReader(new FileReader(input));
		String line;
		while( ((line = reader.readLine())!= null)){ 
			if (line.isEmpty() == true ||	line.charAt(0) == '#' || line.charAt(0) == '%'
				|| line.charAt(0) == '@') {
				continue;
			}
			
			String[] lineSplited = line.split(" ");
			List<Integer> transaction = new ArrayList<Integer>();
			for(String itemString : lineSplited){  
				Integer item = mapValue.get(itemString);
				if(mapSupport.get(item) >= minSupportRelative){
					transaction.add(item);	
				}
			}
			Collections.sort(transaction, new Comparator<Integer>(){
				public int compare(Integer item1, Integer item2){
					int compare = mapSupport.get(item2) - mapSupport.get(item1);
					if(compare == 0){ 
						return (item1 - item2);
					}
					return compare;
				}
			});
			tree.addTransaction(transaction);
		}
		reader.close();
		tree.createHeaderList(mapSupport);
		if(tree.headerList.size() > 0) {
			itemsetBuffer = new int[BUFFERS_SIZE];
			fpNodeTempBuffer = new FPNode[BUFFERS_SIZE];
			fpgrowth(tree, itemsetBuffer, 0, transactionCount, mapSupport);
		}
		if(writer != null){
			writer.close();
		}
		endTime= System.currentTimeMillis();
		return patterns;
	}

	private void fpgrowth(FPTree tree, int [] prefix, int prefixLength, int prefixSupport, Map<Integer, Integer> mapSupport) throws IOException {
		boolean singlePath = true;
		int singlePathSupport = 0;
		int position = 0;
		if(tree.root.childs.size() > 1) {
			singlePath = false;
		}else {
			FPNode currentNode = tree.root.childs.get(0);
			while(true){
				if(currentNode.childs.size() > 1) {
					singlePath = false;
					break;
				}
				fpNodeTempBuffer[position] = currentNode;
				
				position++;
				if(currentNode.childs.size() == 0) {
					break;
				}
				currentNode = currentNode.childs.get(0);
			}
		}
		if(singlePath && singlePathSupport >= minSupportRelative){
			saveAllCombinationsOfPrefixPath(fpNodeTempBuffer, position, prefix, prefixLength);
		}else {
			for(int i = tree.headerList.size()-1; i>=0; i--){
				Integer item = tree.headerList.get(i);
				int support = mapSupport.get(item);
				prefix[prefixLength] = item;
				int betaSupport = (prefixSupport < support) ? prefixSupport: support;
				saveItemset(prefix, prefixLength+1, betaSupport);
				List<List<FPNode>> prefixPaths = new ArrayList<List<FPNode>>();
				FPNode path = tree.mapItemNodes.get(item);
				Map<Integer, Integer> mapSupportBeta = new HashMap<Integer, Integer>();
				
				while(path != null){
					if(path.parent.itemID != -1){
						List<FPNode> prefixPath = new ArrayList<FPNode>();
						prefixPath.add(path);
						int pathCount = path.counter;
						FPNode parent = path.parent;
						while(parent.itemID != -1){
							prefixPath.add(parent);
							if(mapSupportBeta.get(parent.itemID) == null){
								mapSupportBeta.put(parent.itemID, pathCount);
							}else{
								mapSupportBeta.put(parent.itemID, mapSupportBeta.get(parent.itemID) + pathCount);
							}
							parent = parent.parent;
						}
						prefixPaths.add(prefixPath);
					}
					path = path.nodeLink;
				}
				FPTree treeBeta = new FPTree();
				for(List<FPNode> prefixPath : prefixPaths){
					treeBeta.addPrefixPath(prefixPath, mapSupportBeta, minSupportRelative); 
				}
				if(treeBeta.root.childs.size() > 0){
					treeBeta.createHeaderList(mapSupportBeta);
					fpgrowth(treeBeta, prefix, prefixLength+1, betaSupport, mapSupportBeta);
				}
			}
		}
		
	}

	private void saveAllCombinationsOfPrefixPath(FPNode[] fpNodeTempBuffer, int position, 
			int[] prefix, int prefixLength) throws IOException {

		int support = 0;
		for (long i = 1, max = 1 << position; i < max; i++) {
			int newPrefixLength = prefixLength;
			for (int j = 0; j < position; j++) {
				int isSet = (int) i & (1 << j);
				if (isSet > 0) {
					prefix[newPrefixLength++] = fpNodeTempBuffer[j].itemID;
					if(support == 0) {
						support = fpNodeTempBuffer[j].counter;
					}
				}
			}
			saveItemset(prefix, newPrefixLength, support);
		}
	}

	private  Map<Integer, Integer> scanDatabaseToDetermineFrequencyOfSingleItems(String input)
			throws FileNotFoundException, IOException {
		 Map<Integer, Integer> mapSupport = new HashMap<Integer, Integer>();
		BufferedReader reader = new BufferedReader(new FileReader(input));
		String line;
                int idValue = 1;
		while( ((line = reader.readLine())!= null)){
			if (line.isEmpty() == true ||  line.charAt(0) == '#' || line.charAt(0) == '%' 	|| line.charAt(0) == '@') {
				continue;
			}
			String[] lineSplited = line.split(" ");
			for(String itemString : lineSplited){
                                Integer dem = mapValue.get(itemString);
                                if(dem == null){
                                    mapValue.put(itemString, idValue);
                                    mapElse.put(idValue, itemString);
                                    idValue++;
                                }
				Integer item = mapValue.get(itemString);
				Integer count = mapSupport.get(item);
				if(count == null){
					mapSupport.put(item, 1);
				}else{
					mapSupport.put(item, ++count);
				}
			}
			transactionCount++;
		}
		reader.close();
		
		return mapSupport;
	}
	private void saveItemset(int [] itemset, int itemsetLength, int support) throws IOException {
		itemsetCount++;
		if(writer != null){
			System.arraycopy(itemset, 0, itemsetOutputBuffer, 0, itemsetLength);
			Arrays.sort(itemsetOutputBuffer, 0, itemsetLength);
			StringBuilder buffer = new StringBuilder();
			for(int i=0; i< itemsetLength; i++){
				buffer.append(mapElse.get(itemsetOutputBuffer[i]));
				if(i != itemsetLength-1){
					buffer.append(' ');
				}
			}
			buffer.append(" #SUP: ");
			buffer.append(support);
			writer.write(buffer.toString());
			writer.newLine();
                        lstSValue.add(buffer.toString());
			
		}
		else{
			int[] itemsetArray = new int[itemsetLength];
			System.arraycopy(itemset, 0, itemsetArray, 0, itemsetLength);
			Arrays.sort(itemsetArray);
			
			Itemset itemsetObj = new Itemset(itemsetArray);
			itemsetObj.setAbsoluteSupport(support);
			patterns.addItemset(itemsetObj, itemsetLength);
		}
	}

	public void printStats() {
		System.out.println("=============  FP-GROWTH 0.96r19 - STATS =============");
		long temps = endTime - startTimestamp;
		System.out.println(" Transactions count from database : " + transactionCount);
		System.out.println(" Frequent itemsets count : " + itemsetCount); 
		System.out.println(" Total time ~ " + temps + " ms");
		System.out.println("===================================================");
	}

	public int getDatabaseSize() {
		return transactionCount;
	}
}
