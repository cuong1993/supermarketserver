package fpgrowth;

public class AssocRule extends Rule{
	private double lift;
	public AssocRule(int[] itemset1, int[] itemset2, int supportAntecedent,
			int transactionCount, double confidence, double lift) {
		super(itemset1, itemset2, supportAntecedent, transactionCount, confidence);
		this.lift = lift;
	}
        
	public double getLift() {
		return lift;
	}

	public void print() {
		System.out.println(toString());
	}
}
