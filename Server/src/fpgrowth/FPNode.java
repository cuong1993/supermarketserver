package fpgrowth;

import java.util.ArrayList;
import java.util.List;

public class FPNode {
	int itemID = -1; 
	int counter = 1; 

	FPNode parent = null; 
	List<FPNode> childs = new ArrayList<FPNode>();
	FPNode nodeLink = null;
	FPNode(){
		
	}

	FPNode getChildWithID(int id) {
		for(FPNode child : childs){
			if(child.itemID == id){
				return child;
			}
		}
		return null;
	}

	public String toString(String indent) {
		StringBuilder output = new StringBuilder();
		output.append(""+ itemID);
		output.append(" (count="+ counter);
		output.append(")\n");
		String newIndent = indent + "   ";
		for (FPNode child : childs) {
			output.append(newIndent+ child.toString(newIndent));
		}
		return output.toString();
	}
	
	public String toString() {
		return ""+itemID;
	}
}
