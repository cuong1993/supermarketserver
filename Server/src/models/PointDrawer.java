/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;

/**
 *
 * @author Cao
 */
public class PointDrawer implements Serializable{
    public int row = -1;
    public int col = -1;
    public int color = -1;
    public int coordinateX;
    public int coordinateY;
    public boolean isDrawer = false;
}
