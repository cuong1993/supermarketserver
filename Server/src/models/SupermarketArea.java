/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Cao
 */
public class SupermarketArea implements Serializable{
    private int posX = 0;
    private int posY = 0;
    private int angle = 0;
    private int shelfquantity = 0;
    private int shelfheight = 0;
    private int drawernumber = 0;
    private ArrayList<Product> listProducts = null;

    public SupermarketArea() {
    }

    public int getPosX() {
        return posX;
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }

    public int getPosY() {
        return posY;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }

    public int getAngle() {
        return angle;
    }

    public void setAngle(int angle) {
        this.angle = angle;
    }

    public int getShelfquantity() {
        return shelfquantity;
    }

    public void setShelfquantity(int shelfquantity) {
        this.shelfquantity = shelfquantity;
    }

    public int getShelfheight() {
        return shelfheight;
    }

    public void setShelfheight(int shelfheight) {
        this.shelfheight = shelfheight;
    }

    public int getDrawernumber() {
        return drawernumber;
    }

    public void setDrawernumber(int drawernumber) {
        this.drawernumber = drawernumber;
    }

    public ArrayList<Product> getListProducts() {
        return listProducts;
    }

    public void setListProducts(ArrayList<Product> listProducts) {
        this.listProducts = listProducts;
    }

}
