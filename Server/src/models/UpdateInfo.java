/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;

/**
 *
 * @author Cao
 */
public class UpdateInfo implements Serializable{
    public int pos = -1;
    public SupermarketArea area = null;
    public UpdateInfo(int pos, SupermarketArea area)
    {
        this.pos = pos;
        this.area = area;
    }
}
