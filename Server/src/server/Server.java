/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

/**
 *
 * @author Cao
 */
//Example 26 (updated)

import file.FileManager;
import fpgrowth.AlgoAgrawalFaster94;
import fpgrowth.AlgoFPGrowth;
import fpgrowth.Itemsets;
import java.io.DataInputStream;
import java.io.PrintStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.SupermarketArea;
import models.UpdateInfo;

/*
 * A chat server that delivers public and private messages.
 */
public class Server{

  // The server socket.
  private static ServerSocket serverSocket = null;
  // The client socket.
  private static Socket clientSocket = null;

  // This chat server can accept up to maxClientsCount clients' connections.
  private static final int maxClientsCount = 10;
  private static final clientThread[] threads = new clientThread[maxClientsCount];
  private static ArrayList<SupermarketArea> areas = null;

  public static void main(String args[]) {

      FileManager file = new FileManager();
      areas = new ArrayList<SupermarketArea>();
      file.readFile("input.txt", areas);
    // The default port number.
    int portNumber = 2222;
    if (args.length < 1) {
      System.out.println("Usage: java MultiThreadChatServerSync <portNumber>\n"
          + "Now using port number=" + portNumber);
    } else {
      portNumber = Integer.valueOf(args[0]).intValue();
    }

    /*
     * Open a server socket on the portNumber (default 2222). Note that we can
     * not choose a port less than 1023 if we are not privileged users (root).
     */
    try {
      serverSocket = new ServerSocket(portNumber);
    } catch (IOException e) {
      System.out.println(e);
    }

    /*
     * Create a client socket for each connection and pass it to a new client
     * thread.
     */
    while (true) {
      try {
        clientSocket = serverSocket.accept();
        int i = 0;
        for (i = 0; i < maxClientsCount; i++) {
          if (threads[i] == null) {
            (threads[i] = new clientThread(clientSocket, threads, areas)).start();
            break;
          }
        }
        if (i == maxClientsCount) {
          PrintStream os = new PrintStream(clientSocket.getOutputStream());
          os.println("Server too busy. Try later.");
          os.close();
          clientSocket.close();
        }
      } catch (IOException e) {
        System.out.println(e);
      }
    }
  }
}

/*
 * The chat client thread. This client thread opens the input and the output
 * streams for a particular client, ask the client's name, informs all the
 * clients connected to the server about the fact that a new client has joined
 * the chat room, and as long as it receive data, echos that data back to all
 * other clients. The thread broadcast the incoming messages to all clients and
 * routes the private message to the particular client. When a client leaves the
 * chat room this thread informs also all the clients about that and terminates.
 */
class clientThread extends Thread {

  private String clientName = null;
  private ObjectInputStream is = null;
  private ObjectOutputStream os = null;
  private Socket clientSocket = null;
  private final clientThread[] threads;
  private int maxClientsCount;
  private ArrayList<SupermarketArea> areas = null;

  public clientThread(Socket clientSocket, clientThread[] threads,ArrayList<SupermarketArea> areas) {
    this.clientSocket = clientSocket;
    this.threads = threads;
    this.areas = areas;
    maxClientsCount = threads.length;
  }

  public void run() {
    int maxClientsCount = this.maxClientsCount;
    clientThread[] threads = this.threads;
    try {
      /*
       * Create input and output streams for this client.
       */
      is = new ObjectInputStream(clientSocket.getInputStream());
      os = new ObjectOutputStream(clientSocket.getOutputStream());
      String name = clientSocket.getInetAddress().toString();

      /* Welcome the new the client. */
      synchronized (this) {
        for (int i = 0; i < maxClientsCount; i++) {
          if (threads[i] != null && threads[i] == this) {
            clientName = "@" + name;
            threads[i].os.writeObject("area_info");
            threads[i].os.writeObject(this.areas);
            threads[i].os.reset();
            break;
          }
        }
      }
      /* Start the conversation. */
      while (true) {
        String line = (String)is.readObject();
        if (line.startsWith("/quit")) {
          break;
        }
        if(line.equals("update"))
        {
            UpdateInfo info = (UpdateInfo)is.readObject();
            areas.remove(info.pos);
            areas.add(info.pos, info.area);
            FileManager file = new FileManager();
            file.WriteFile("input.txt", areas);
            /* The message is public, broadcast it to all other clients. */
          synchronized (this) {
            for (int i = 0; i < maxClientsCount; i++) {
              if (threads[i] != null && threads[i].clientName != null) {
                threads[i].os.writeObject("area_info");
                threads[i].os.writeObject(areas);
                threads[i].os.reset();
              }
            }
          }
        }
        else if(line.equals("hd"))
        {
            ArrayList<String> lstS = (ArrayList<String>)is.readObject();
            FileManager file = new FileManager();
            file.OuputFile("transaction.txt", lstS);
        }
        else if(line.equals("Frequent"))
        {
            String input = "transaction.txt";
            String output = "frequent.txt";
            String sMin = (String)is.readObject();
            String []sMinValue = sMin.split(",");
            // STEP 1: Applying the FP-GROWTH algorithm to find frequent itemsets
            double minsupp = Double.parseDouble(sMinValue[0]);
            AlgoFPGrowth fpgrowth = new AlgoFPGrowth();
            fpgrowth.runAlgorithm(input, output, minsupp);
            os.writeObject("Frequent");
            os.writeObject(AlgoFPGrowth.lstSValue);
            AlgoFPGrowth.lstSValue.removeAll(AlgoFPGrowth.lstSValue);
        }
        else if(line.equals("Associations"))
        {
            String input = "transaction.txt";
            String output = "associations.txt";
            String sMin = (String)is.readObject();
            String []sMinValue = sMin.split(",");
            // STEP 1: Applying the FP-GROWTH algorithm to find frequent itemsets
            double minsupp = Double.parseDouble(sMinValue[0]);
            AlgoFPGrowth fpgrowth = new AlgoFPGrowth();
            Itemsets patterns = fpgrowth.runAlgorithm(input, null, minsupp);
            int databaseSize = fpgrowth.getDatabaseSize();

            // STEP 2: Generating all rules from the set of frequent itemsets (based on Agrawal & Srikant, 94)
            double  minconf = Double.parseDouble(sMinValue[1]);
            AlgoAgrawalFaster94 algoAgrawal = new AlgoAgrawalFaster94();
            algoAgrawal.runAlgorithm(patterns, output, databaseSize, minconf);
            os.writeObject("Associations");
            os.writeObject(AlgoAgrawalFaster94.lstSValue);
            AlgoAgrawalFaster94.lstSValue.removeAll(AlgoAgrawalFaster94.lstSValue);
        }
      }
      os.writeObject("bye");

      /*
       * Clean up. Set the current thread variable to null so that a new client
       * could be accepted by the server.
       */
      synchronized (this) {
        for (int i = 0; i < maxClientsCount; i++) {
          if (threads[i] == this) {
            threads[i] = null;
          }
        }
      }
      /*
       * Close the output stream, close the input stream, close the socket.
       */
      is.close();
      os.close();
      clientSocket.close();
    } catch (IOException e) {
    } catch (ClassNotFoundException ex) {
          Logger.getLogger(clientThread.class.getName()).log(Level.SEVERE, null, ex);
      }
  }
}
